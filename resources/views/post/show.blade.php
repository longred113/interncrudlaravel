@extends('post.layout')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2 class="col-xs-12 col-sm-12 col-md-12 text-center"> Show User</h2>
            </div>
        </div>
    </div>
    <form method="post" action="{{route('post.update',$post->_id)}}">
    @csrf
    @method('GET')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <div class="form-group">
                <strong>Title:</strong>
                {{ $post->title }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <div class="form-group">
                <strong>User name:</strong>
                {{ $post->username }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <div class="form-group">
                <strong>Content:</strong>
                {{ $post->content }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <a class="btn btn-primary" href="{{ route('post.index') }}"> Back</a>
        </div>
    </div>
    </form>
@endsection