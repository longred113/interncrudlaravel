@extends('post.layout')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2 class="col-xs-12 col-sm-12 col-md-12 text-center">Add New User</h2>
        </div>
    </div>
</div>
<form method="post" action="{{route('post.store')}}">
    @csrf
    <div class="form-group">
        <label for="title">Title:</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Title">
    </div>
    <div class="form-group">
        <label for="username">User name:</label>
        <input type="text" class="form-control" id="username" name="username" placeholder="Username">
    </div>
    <div class="form-group">
        <label for="content">Content:</label>
        <input type="text" class="form-control" id="content" name="content" placeholder="Content">
    </div >
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Send</button>
        <a class="btn btn-primary" href="{{ route('post.index') }}"> Back </a>
    </div>
</form>