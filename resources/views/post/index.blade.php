@extends('post.layout')


@section('content')
<h3 style="text-align: center">All User</h3>
<table class="table">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Title</th>
            <th scope="col">User name</th>
            <th scope="col">Content</th>
            <th scope="col">Status</th>
            <th>
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <a class="btn btn-success" href="{{ route('post.create') }}"> Create New User</a>
                        </div>
                    </div>
                </div>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $key=>$item)
        <tr>
            <th scope="row">{{$key}}</th>
            <td>{{$item->title}}</td>
            <td>{{$item->username}}</td>
            <td>{{$item->content}}</td>
            <td>
                <form method="post" action="{{route('post.destroy',$item)}} ">
                    <a class="btn btn-info" href="{{route('post.show',$item)}}"> Show</a>
                    <a class="btn btn-primary" href="{{route('post.edit',$item)}}">Edit</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" method="DELETE" class="btn btn-danger">Delete</button>
                </form>
            </td>
            <td></td>
        </tr>
        @endforeach
    </tbody>
</table>