<?php

namespace App\Repositories;

use App\Repositories\EloquentRepository;

class PostRepository extends EloquentRepository
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return \App\Models\Post::class;
    }
}
