<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Repositories\PostRepository;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $PostRepository;
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }
    public function index()
    {
        // $data = Post::all();
        // return view('post.index',compact('data'));

        $data = $this->postRepository->getAll();
        return view('post.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Post::create($request->all());
        // return redirect()->route('post.index') 
        // ->with('succcess', 'user create succcessfully');

        $data = $request->all();

        $post = $this->postRepository->create($data);
        return redirect()->route('post.index',compact('post'));  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {

        // $post = $this->postRepository->find($post);
        $id = Post::has('id')->get();   
        dd($id);
        return view('post.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('post.edit',compact('post'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        // request()->validate([
        //     'title' => 'required',
        //     'username' => 'required',
        //     'content' => 'required',
        // ]);

        // $post->update($request->all());
        // return redirect()->route('post.index')
        // ->with('succcess', 'user update succcessfully');

        $data = $request->all();
        // dd($data);
        $post = $this->postRepository->update($post->_id, $data);
        return redirect()->route('post.index',compact('post'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        // $post->delete();
        // return redirect()->route('post.index')
        // ->with('succcess', 'user delete succcessfully');

        $this->postRepository->delete($post->_id);
        return redirect()->route('post.index');
    }
}
